# VidMerge
Takes input video and audio file then copies them together with ffmpeg, outputting video-processed.mkv or video-processed.mp4  
Useful for carrying out audio processing effects with Audacity to a video, and then merging the processed audio file back with the video

Usage `bash vidmerge.sh video`